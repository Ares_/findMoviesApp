import 'package:bloc/bloc.dart';
import 'package:find_your_movie/models/movie.dart';
import 'package:find_your_movie/repositories/repository.dart';

class MovieCubit extends Cubit<List<Movie>> {
  MovieCubit(this._repository) : super([]);
  final Repositories _repository;

  Future<void> loadFavorites() async {
    final List<Movie> movies = await _repository.loadFavorites();
    emit(movies);
  }

  void saveFavorites(List<Movie> movies) {
    _repository.saveFavorite(movies);
  }

  void addToFavorite(Movie movie) {
    emit([...state, movie]);
    _repository.saveFavorite(state);
  }
}
