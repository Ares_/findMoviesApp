import 'package:find_your_movie/models/movie.dart';
import 'package:find_your_movie/repositories/repository.dart';
import 'package:find_your_movie/ui/widgets/movie_widget.dart';
import 'package:find_your_movie/ui/screens/favorite_screen.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:provider/provider.dart';

class Movies extends StatefulWidget {
  const Movies({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _Movies();
}

class _Movies extends State<Movies> {
  int _selectedPageIndex = 0;
  final RefreshController _refreshController = RefreshController();

  final Widget _moviesPage = _MoviesPage();
  final Widget _myListPage = const MyListPage();

  Widget getSelectedPage() {
    return _selectedPageIndex == 0 ? _moviesPage : _myListPage;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: getSelectedPage(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: const Color(0xFF24052C),
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.movie),
            label: "Movies",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.favorite,
            ),
            label: "My list",
          )
        ],
        currentIndex: _selectedPageIndex,
        selectedItemColor: const Color(0xFFFFA000),
        selectedIconTheme: const IconThemeData(color: Color(0xFFFFA000)),
        unselectedItemColor: Colors.white,
        onTap: (int index) => {
          setState(() {
            _selectedPageIndex = index;
          })
        },
      ),
    );
  }
}

class _MoviesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          /*
          TODO implement a SmartRefresher to reload random Movie
        SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: const WaterDropHeader(),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        ),
          */
          FutureBuilder<Movie>(
              future:
                  Provider.of<Repositories>(context, listen: false).loadRandom(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Text("Loading...");
                }
                return MovieWidget(snapshot.data!);
              })
        ],
      ),
    );
  }
}

RefreshController _refreshController = RefreshController(initialRefresh: false);

void _onRefresh() async {
  // monitor network fetch
  await Future.delayed(const Duration(milliseconds: 1000));
  // if failed,use refreshFailed()
  _refreshController.refreshCompleted();
}

void _onLoading() async {
  // monitor network fetch
  await Future.delayed(const Duration(milliseconds: 1000));

  _refreshController.loadComplete();
}
