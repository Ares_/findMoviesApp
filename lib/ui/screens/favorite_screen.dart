import 'package:cached_network_image/cached_network_image.dart';
import 'package:find_your_movie/blocs/movie_cubit.dart';
import 'package:find_your_movie/models/movie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'movie_info.dart';

class MyListPage extends StatefulWidget {
  const MyListPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyListPage();
}

class _MyListPage extends State<MyListPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieCubit, List<Movie>>(builder: (context, _movies) {
      return SingleChildScrollView(
          child: Container(
        color: const Color(0xFF24052C),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          ListView.separated(
            padding:
                const EdgeInsets.only(bottom: 15, top: 15, left: 10, right: 10),
            itemCount: _movies.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  showTopSnackBar(
                    context,
                    CustomSnackBar.success(
                        backgroundColor: const Color(0xFF5C166E),
                        message:
                            ('${_movies[index].title} removed from my list'),
                        messagePadding:
                            const EdgeInsets.symmetric(horizontal: 5)),
                    displayDuration: const Duration(milliseconds: 1000),
                    showOutAnimationDuration: const Duration(milliseconds: 800),
                    hideOutAnimationDuration: const Duration(milliseconds: 400),
                  );

                  setState(() {
                    _movies.removeAt(index);
                    Provider.of<MovieCubit>(context, listen: false)
                        .saveFavorites(_movies);
                  });
                },
                child: SizedBox(
                    height: 200,
                    child: Container(
                      decoration: const BoxDecoration(boxShadow: [
                        BoxShadow(
                          spreadRadius: 0.1,
                          color: Colors.white70,
                          blurRadius: 1.0,
                        ),
                      ]),
                      child: Card(
                        shape: const ContinuousRectangleBorder(
                          borderRadius: BorderRadius.zero,
                        ),
                        elevation: 220,
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, MovieInfo.route,
                                arguments: {"movies": _movies[index]});
                          },
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                  flex: 3, // 30%
                                  child: Container(
                                    constraints: const BoxConstraints.expand(),
                                    color: const Color(0xFF24052C),
                                    child: CachedNetworkImage(
                                      imageUrl: _movies[index].imgUrl ?? '',
                                      placeholder: (context, url) =>
                                          const CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          const Icon(Icons.error),
                                    ),
                                  )),
                              Flexible(
                                  flex: 7, // 70%
                                  child: ListTile(
                                      title: Text(_movies[index].title ?? ''),
                                      subtitle: Text(
                                          _movies[index].description ?? '')))
                            ],
                          ),
                        ),
                      ),
                    )),
              );
            },
            separatorBuilder: (context, index) => const SizedBox(
              height: 15,
            ),
          ),
        ]),
      ));
    });
  }
}
