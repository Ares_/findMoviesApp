import 'package:find_your_movie/models/movie.dart';
import 'package:find_your_movie/ui/widgets/movie_widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MovieInfo extends StatelessWidget {
  const MovieInfo({Key? key}) : super(key: key);
  static const route = "/movie-info";

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args =
        ModalRoute.of(context)?.settings.arguments as Map<String, Movie>;
    final Movie movie = args['movies'];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF24052C),
        title: Text(movie.title.toString(),
            style: GoogleFonts.mcLaren(
                fontSize: 18,
                fontWeight: FontWeight.normal,
                color: Colors.white),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: Colors.white,
          onPressed: () {
            Navigator.pop(context);
          },
          iconSize: 18,
        ),
      ),
      body:
          SafeArea(child: SingleChildScrollView(child: MovieWidget(movie))),
    );
  }
}
