import 'package:google_fonts/google_fonts.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:find_your_movie/blocs/movie_cubit.dart';
import 'package:find_your_movie/models/movie.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MovieWidget extends StatelessWidget {
  const MovieWidget(this.movie, {Key? key}) : super(key: key);
  final Movie movie;

  @override
  Widget build(BuildContext context) {
    List<Movie> favorites =
        Provider.of<MovieCubit>(context, listen: false).state;
    return Column(
      children: [
        CachedNetworkImage(
          imageUrl: movie.imgUrl ?? '',
          placeholder: (context, url) => const CircularProgressIndicator(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
        Column(children: [
          ListTile(
            title: Column(children: [
              Wrap(
                children: [
                  Text(
                    movie.title ?? 'Title not found',
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.mcLaren(
                        fontSize: 18, fontWeight: FontWeight.normal),
                  ),
                  StarButton(
                      iconSize: 36,
                      isStarred: favorites.contains(movie),
                      valueChanged: (_isFavorite) {
                        if (_isFavorite) {
                          Provider.of<MovieCubit>(context, listen: false)
                              .addToFavorite(movie);
                        } else {
                          favorites.removeAt(favorites
                              .indexWhere((element) => element.id == movie.id));
                          Provider.of<MovieCubit>(context, listen: false)
                              .saveFavorites(favorites);
                        }
                      })
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 10.0, bottom: 10.0),
                    child: Text(
                        'Vote average : ${movie.voteAverage != 0.0 ? movie.voteAverage : 'N/A'}'),
                  )
                ],
              ),
            ]),
            subtitle: Text(movie.description ?? 'Unknown'),
          ),
        ])
      ],
    );
  }
}
