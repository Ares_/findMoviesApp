import 'package:find_your_movie/blocs/movie_cubit.dart';
import 'package:find_your_movie/repositories/movie_repository.dart';
import 'package:find_your_movie/repositories/preferences_repository.dart';
import 'package:find_your_movie/repositories/repository.dart';
import 'package:find_your_movie/ui/screens/movie_info.dart';
import 'package:find_your_movie/ui/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FindMoviesApp extends StatelessWidget {
  const FindMoviesApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Find your movie',
      theme: ThemeData(
        backgroundColor: const Color(0xFF24052C),
      ),
      initialRoute: '/movies',
      routes: {
        '/movies': (context) => const Movies(),
        '/movie-info': (context) => const MovieInfo(),
      },
    );
  }
}
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final PreferencesRepository preferencesRepository = PreferencesRepository();
  final MovieRepository movieRepository = MovieRepository();
  final Repositories repository =
  Repositories(movieRepository, preferencesRepository);
  final MovieCubit movieCubit = MovieCubit(repository);

  await repository.loadRandom();
  await movieCubit.loadFavorites();

  runApp(MultiProvider(
    providers: [
      Provider<MovieCubit>(create: (_) => movieCubit),
      Provider<Repositories>(create: (_) => repository),
    ],
    child: const FindMoviesApp(),
  ));
}
