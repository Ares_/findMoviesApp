import 'dart:convert';

class Movie {
  int id;
  String? title;
  String? originalTitle;
  String? description;
  String? imgUrl;
  double? voteAverage;

  Movie(this.id, this.title, this.originalTitle, this.description, this.imgUrl,
      this.voteAverage);

  /// Hydrate data from API
  factory Movie.fromJsonApi(Map<String, dynamic> map) {
    int _id = map['id'];
    String? _title = map['title'] ?? map['name'] ?? map['originalTitle'];
    String? _originalTitle = map['originalTitle'];
    String? _description = map['overview'];
    double? _voteAverage = map['vote_average'];
    String? _imgUrl = 'https://image.tmdb.org/t/p/w500/' + map['poster_path'];

    return Movie(
        _id, _title, _originalTitle, _description, _imgUrl, _voteAverage);
  }

  factory Movie.fromJsonPreferences(String json) {
    Map<String, dynamic> map = jsonDecode(json);

    return Movie(map['id'], map['title'], map['originalTitle'],
        map['description'], map['imgUrl'], map['voteAverage']);
  }

  String toJson() {
    return jsonEncode({
      'id': id,
      'title': title,
      'description': description,
      'imgUrl': imgUrl,
      'voteAverage': voteAverage,
    });
  }
}
