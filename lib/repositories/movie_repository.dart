import 'dart:convert';
import 'dart:math';
import 'package:find_your_movie/models/movie.dart';
import 'package:http/http.dart';
import 'dart:developer' as developer;

class MovieRepository {
  Future<Movie> fetchRandom() async {
    var uri = Uri.https('api.themoviedb.org', '/3/trending/all/day', {
      'api_key': 'b9d59c13c238fb8ba63902971a49a128',
      'page': getRandomPage().toString()
    });
    //
    Response response = await get(uri);
    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      return Movie.fromJsonApi(json['results'][getRandomIndex()]);
    } else {
      developer.log(jsonDecode(response.body));
      throw Exception('Failed to fetch random movies');
    }
  }

  int getRandomPage() {
    var randomPage = Random().nextInt(10);
    return randomPage;
  }

  int getRandomIndex() {
    var randomMovie = Random().nextInt(19);
    return randomMovie;
  }
}
