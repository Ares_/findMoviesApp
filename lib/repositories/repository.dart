import 'package:find_your_movie/models/movie.dart';
import 'package:find_your_movie/repositories/movie_repository.dart';
import 'package:find_your_movie/repositories/preferences_repository.dart';

class Repositories {
  final PreferencesRepository _preferencesRepository;
  final MovieRepository _movieRepository;

  Future<Movie> loadRandom() => _movieRepository.fetchRandom();

  Future<List<Movie>> loadFavorites() async =>
      _preferencesRepository.loadFavorite();

  Future<void> saveFavorite(List<Movie> movies) async =>
      _preferencesRepository.saveMovies(movies);

  Repositories(this._movieRepository, this._preferencesRepository);
}
