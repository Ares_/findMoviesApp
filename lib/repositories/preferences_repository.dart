import 'package:find_your_movie/models/movie.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveMovies(List<Movie> movies) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> _moviesList = [];
    for (var element in movies) {
      _moviesList.add(element.toJson());
    }
    prefs.setStringList('movies', _moviesList);
  }

  Future<List<Movie>> loadFavorite() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? prefsList = prefs.getStringList('movies');
    List<Movie> moviesList = [];
    prefsList?.forEach((element) {
      if (element.isNotEmpty) {
        moviesList.add(Movie.fromJsonPreferences(element));
      }
    });
    return moviesList;
  }

}
